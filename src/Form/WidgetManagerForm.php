<?php

namespace Drupal\lytics\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\lytics\Entity\LyticsWidget;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Cache\Cache;

class WidgetManagerForm extends FormBase
{

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new WidgetManagerForm object.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(MessengerInterface $messenger)
  {
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'widget_manager_form';
  }

  /**
   * Form submission handler for the delete button.
   */
  public function deleteWidget(array &$form, FormStateInterface $form_state)
  {
    $values = $form_state->getValues();
    $id = $values['id'];
    $widget = LyticsWidget::load($id);
    if ($widget) {
      $widget->delete();
      $this->messenger->addMessage($this->t('Widget deleted successfully.'));
    } else {
      $this->messenger->addError($this->t('Unable to delete the widget. It may not exist.'));
    }

    $form_state->setRedirect('lytics.manage_web_widgets');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, LyticsWidget $lytics_widget = NULL)
  {
    $config = \Drupal::config('lytics.settings');
    $accountID = $config->get('account_id');
    $apitoken = $config->get('apitoken');
    $config = $lytics_widget ? $lytics_widget->getConfiguration() : '';

    function getAvailableAudiences($apitoken, $table, $onlyPublic)
    {
      $url = "https://api.lytics.io/v2/segment?table=".$table."&valid=true&kind=segment";

      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: ' . $apitoken,
      ));

      $response = curl_exec($ch);

      if ($response === false) {
        error_log("Error: " . curl_error($ch));
        return [];
      }

      $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
      if ($httpCode >= 400) {
        error_log("Request failed with status: " . $response);
        return [];
      }

      curl_close($ch);

      $data = json_decode($response, true);

      if ($data === null || !isset($data['data']) || !is_array($data['data'])) {
        error_log("Unexpected response format");
        return [];
      }

      $allSegments = $data['data'];
      $publicSegments = array_filter($allSegments, function ($segment) {
        return $segment['is_public'] === true;
      });

      $filteredSegments = [];
      if($onlyPublic){
        $filteredSegments = $publicSegments;
      } else {
        $filteredSegments = $allSegments;
      }

      $options = array_map(function ($segment) {
        return array(
          'label' => $segment['name'],
          'value' => $segment['slug_name'],
          'type' => 'string'
        );
      }, $filteredSegments);

      usort($options, function ($a, $b) {
        return strcmp($a['label'], $b['label']);
      });

      return $options;
    }

    $audiences = getAvailableAudiences($apitoken, 'user', true);
    $collections = getAvailableAudiences($apitoken, 'content', false);

    $form['id'] = [
      '#type' => 'hidden',
      '#value' => $lytics_widget ? $lytics_widget->id() : NULL,
    ];

    $form['title'] = [
      '#type' => 'hidden',
      '#title' => $this->t('Title'),
      '#required' => TRUE,
      '#default_value' => $lytics_widget ? $lytics_widget->getTitle() : '',
      '#attributes' => [
        'id' => 'edit-title',
      ],
    ];

    $form['description'] = [
      '#type' => 'hidden',
      '#title' => $this->t('Description'),
      '#rows' => 2,
      '#required' => FALSE,
      '#default_value' => $lytics_widget ? $lytics_widget->getDescription() : '',
      '#attributes' => [
        'id' => 'edit-description',
      ],
    ];

    $form['status'] = [
      '#type' => 'hidden',
      '#title' => $this->t('Status'),
      '#options' => [
        'draft' => $this->t('Draft'),
        'review' => $this->t('Review'),
        'published' => $this->t('Published'),
        'paused' => $this->t('Paused'),
      ],
      '#attributes' => [
        'id' => 'edit-status',
      ],
      '#default_value' => $lytics_widget ? $lytics_widget->getStatus() : 'draft',
    ];

    $form['config'] = [
      '#type' => 'hidden',
      '#title' => $this->t('Configuration'),
      '#required' => FALSE,
      '#rows' => 10,
      '#element_validate' => [
        [$this, 'validateJson'],
      ],
      '#default_value' => $lytics_widget ? $lytics_widget->getConfiguration() : '',
      '#attributes' => [
        'id' => 'edit-configuration',
      ],
    ];

    $form['widget_wizard'] = [
      '#markup' => Markup::create('
        <lytics-widgetwiz accountid="' . $accountID . '" accesstoken="' . $apitoken . '" pathforaconfig="' . base64_encode($config) . '" availableaudiences="' . base64_encode(json_encode($audiences)) . '" availablecollections="' . base64_encode(json_encode($collections)) . '" titlefield="edit-title" descriptionfield="edit-description" statusfield="edit-status" configurationfield="edit-configuration" >
        </lytics-widgetwiz>'),
      '#attached' => [
        'library' => [
          'lytics/lytics-web-component',
        ],
      ],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#attributes' => [
        'style' => 'background: linear-gradient(88.28deg, #29183E 39.14%, #0E5BA3 99.68%); color: white; border: none; padding: 15px 50px; border-radius: 5px; float: right;',
      ],
    ];

    $form['actions']['delete'] = [
      '#type' => 'submit',
      '#value' => $this->t('Delete'),
      '#submit' => ['::deleteWidget'],
      '#attributes' => [
        'style' => 'background-color: red; color: white; border: none; padding: 15px 20px; border-radius: 5px; float: left;',
      ],
    ];


    return $form;
  }

  // Validate that the JSON meets minimum required format
  public function validateJson($element, FormStateInterface $form_state)
  {
    // has id
    // has name
    // has status
    // $value = $form_state->getValue('configuration');
    // if (!empty($value)) {
    //   $decoded = json_decode($value);
    //   if ($decoded === null) {
    //     $form_state->setError($element, $this->t('The configuration must be valid JSON.'));
    //   }
    // }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $values = $form_state->getValues();

    // If an entity ID is present, load the entity for editing; otherwise, create a new one.
    $entity = !empty($values['id']) ? LyticsWidget::load($values['id']) : LyticsWidget::create();
    $entity->setTitle($values['title']);
    $entity->setDescription($values['description']);
    $entity->setConfiguration($values['config']);
    $entity->setStatus($values['status']);
    $entity->save();

    $this->messenger->addMessage($this->t('Lytics widget saved successfully. If you have made changes to a published widget you may need to clear the Drupal cache for changes to be reflected.'));

    // Bust cache.
    Cache::invalidateTags(['rendered', 'rendered:themes']);

    $form_state->setRedirect('lytics.manage_web_widgets');
  }
}