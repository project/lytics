<?php

namespace Drupal\lytics\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Render\Markup;


/**
 * Provides a 'Content Recommendation' Block from Lytics.
 *
 * @Block(
 *   id = "lytics_content_recommendation_block",
 *   admin_label = @Translation("Lytics Content Recommendation"),
 *   category = @Translation("Lytics"),
 * )
 */
class LyticsContentRecommendationBlock extends BlockBase implements ContainerFactoryPluginInterface
{
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function build()
  {
    $settings = \Drupal::config('lytics.settings')->get();
    $accountID = $settings['account_id'];

    $config = $this->getConfiguration();
    $id = rand(1, 1000);
    $recommendationType = $config['recommendation_type'] ?? 'individual';
    $contentCollection = $config['content_collection'] ?? 'content_with_images';
    $interestEngineId = $config['interest_engine_id'] ?? '';
    $segmentId = $config['segment_id'] ?? '';
    $url = $config['url'] ?? '';
    $numberOfRecommendations = $config['number_of_recommendations'] ?? 3;
    $shuffleResults = $config['dont_shuffle_results'] ?? false;
    $excludeViewedContent = $config['include_viewed_content'] ?? false;

    // Build your block content here.
    $textString = '<div class="loading-message"><div class="loader"></div></div>';

    // Build config base64
    $options = [
      'account_id' => $accountID,
      'element' => 'rec-container-' . $id,
      'block_id' => $id,
      'recommendation_type' => $recommendationType,
      'content_collection_id' => $contentCollection,
      'interest_engine_id' => $interestEngineId,
      'segment_id' => $segmentId,
      'url' => $url,
      'number_of_recommendations' => $numberOfRecommendations,
      'dont_shuffle_results' => $shuffleResults,
      'include_viewed_content' => $excludeViewedContent,
    ];
    $options = base64_encode(json_encode($options));

    return [
      '#markup' => Markup::create('<div class="flex-container justify-between align-stretch flex-wrap gap-medium" id="rec-container-' . $id . '" data-rec-config="' . $options . '">' . $textString . '</div>'),
      '#attached' => [
        'library' => [
          'lytics/lytics-inline-recommendation',
          'lytics/lytics-styles',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state)
  {
    // Get token
    $settingsConfig = \Drupal::config('lytics.settings')->get();
    $apiToken = $settingsConfig['apitoken'];

    // Get block config
    $config = $this->getConfiguration();
    $form = parent::blockForm($form, $form_state);

    // Content Recommendation Type Selection
    $form['recommendation_type'] = [
      '#type' => 'select',
      '#title' => $this->t('What kind of recommendation would you like to make?'),
      '#empty_option' => $this->t('Choose Recommendation Type'),
      '#options' => [
        'individual' => $this->t('Recommend content that is relevant to each visitor.'),
        // 'audience' => $this->t('I want to recommend content that is relevant to a specific audience.'),
        // 'location' => $this->t('I want to recommend content relevant to the page the visitor is on.'),
      ],
      '#attributes' => [
        'id' => 'type-field',
        'style' => 'min-width: 515px;',
      ],
      '#default_value' => $config['recommendation_type'] ?? 'individual',
    ];

    // Recommendation Target
    $form['recommendation_modeling'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Model Selection'),
      '#description' => $this->t('Select the interest engine and content collection to power the recommendations.'),
    ];

    // Interest Engine ID
    $form['recommendation_modeling']['interest_engine_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Interest Engine to Power Recommendation'),
      '#description' => $this->t('Enter the ID of the interest engine to power the recommendations.'),
      '#default_value' => $config['interest_engine_id'] ?? '',
      '#empty_option' => $this->t('Select an Interest Engine'),
      '#options' => $this->getInterestEngines($apiToken),
      '#attributes' => [
        'style' => 'min-width: 525px;',
      ],
    ];

    // Content Collection
    $form['recommendation_modeling']['content_collection'] = [
      '#type' => 'select',
      '#title' => $this->t('Which Content Collection would we recommend from?'),
      '#description' => $this->t('Content Collections allow you to control the rules for what content is recommended. For more detail on how to create and manage Content Collections, visit the <a href=":url" target="_blank">Lytics documentation</a>.', [':url' => 'https://docs.lytics.com/docs/content-collections#building-a-content-collection']),
      '#default_value' => $config['content_collection'] ?? 'content_with_images',
      '#empty_option' => $this->t('Select a Content Collection'),
      '#options' => $this->getContentCollectionOptions($apiToken),
      '#attributes' => [
        'style' => 'min-width: 525px;',
      ],
    ];

    // Type Based Settings
    $form['recommendation_details'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Recommendation Source'),
      '#description' => $this->t('Required details based on the selected recommendation recommendation type.'),
      '#states' => [
        'visible' => [
          ':input[id="type-field"]' => [
            ['value' => 'audience'],
            ['value' => 'location'],
          ],
        ],
      ],
      '#attributes' => [
        'style' => 'min-width: 525px;',
      ],
    ];

    // Audience Selection
    $form['recommendation_details']['segment_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Audience'),
      '#description' => $this->t('Enter the audience for audience-based recommendations.'),
      '#default_value' => $config['segment_id'] ?? '',
      '#empty_option' => $this->t('Select an Audience'),
      '#options' => $this->getSegments($apiToken),
      '#states' => [
        'visible' => [
          ':input[id="type-field"]' => ['value' => 'audience'],
        ],
      ],
      '#attributes' => [
        'style' => 'min-width: 525px;',
      ],
    ];

    // URL Field
    $form['recommendation_details']['url'] = [
      '#type' => 'url',
      '#title' => $this->t('URL'),
      '#default_value' => $config['url'] ?? '',
      '#description' => $this->t('Enter the URL for URL-based recommendations.'),
      '#states' => [
        'visible' => [
          ':input[id="type-field"]' => ['value' => 'location'],
        ],
      ],
      '#attributes' => [
        'style' => 'min-width: 525px;',
      ],
    ];

    // Recommendation Options
    $form['recommendation_options'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Options'),
      '#description' => $this->t('Optional settings to further customize your recommendations.'),
    ];

    // Number of Recommendations
    $form['recommendation_options']['number_of_recommendations'] = [
      '#type' => 'select',
      '#title' => $this->t('How many recommendations would you like to show?'),
      '#description' => $this->t('The number of recommendations to show in the block.'),
      '#default_value' => $config['number_of_recommendations'] ?? 3,
      '#options' => [
        1 => 1,
        2 => 2,
        3 => 3,
        4 => 4,
        5 => 5,
      ],
    ];

    // Shuffle Results
    $form['recommendation_options']['dont_shuffle_results'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Do Not Shuffle Results'),
      '#default_value' => $config['dont_shuffle_results'] ?? false,
      '#description' => $this->t('By default we shuffle the recommendations so that visitors see different content each time they visit. Uncheck this box to disable shuffling.'),
    ];

    // Exclude Recently Viewed Content
    $form['recommendation_options']['include_viewed_content'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include Recently Viewed Content'),
      '#default_value' => $config['include_viewed_content'] ?? false,
      '#description' => $this->t('By default we exclude content that the visitor has recently viewed. Check this box to include recently viewed content in the recommendations.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state)
  {
    $config = $this->getConfiguration();
    $config['recommendation_type'] = $form_state->getValue('recommendation_type');
    $config['interest_engine_id'] = $form_state->getValue(['recommendation_modeling', 'interest_engine_id']);
    $config['content_collection'] = $form_state->getValue(['recommendation_modeling', 'content_collection']);
    $config['segment_id'] = $form_state->getValue(['recommendation_details', 'segment_id']);
    $config['url'] = $form_state->getValue(['recommendation_details', 'url']);
    $config['number_of_recommendations'] = $form_state->getValue(['recommendation_options', 'number_of_recommendations']);
    $config['dont_shuffle_results'] = $form_state->getValue(['recommendation_options', 'dont_shuffle_results']);
    $config['include_viewed_content'] = $form_state->getValue(['recommendation_options', 'include_viewed_content']);
    $this->configuration = $config;
  }

  /**
   * Helper function to fetch content collection options.
   */
  public function getContentCollectionOptions($apiToken)
  {
    // Get available collections.
    $url = 'https://api.lytics.io/v2/segment?table=content';
    $response = \Drupal::httpClient()->get($url, [
      'headers' => [
        'Authorization' => $apiToken,
      ],
    ]);

    $data = json_decode($response->getBody(), TRUE);
    $options = [];

    foreach ($data['data'] as $collection) {
      $options[$collection['slug_name']] = $collection['name'];
    }

    asort($options, SORT_STRING | SORT_FLAG_CASE);

    return $options;
  }

  /**
   * Helper function to fetch interest engines.
   */
  public function getInterestEngines($apiToken)
  {
    // Get available engines.
    $url = 'https://api.lytics.io/api/content/config';
    $response = \Drupal::httpClient()->get($url, [
      'headers' => [
        'Authorization' => $apiToken,
      ],
    ]);

    $data = json_decode($response->getBody(), TRUE);
    $options = [];

    foreach ($data['data'] as $engine) {
      if (empty($engine['label'])) {
        $engine['label'] = 'Default (Recommended)';
      }

      $options[$engine['id']] = $engine['label'];
    }

    asort($options, SORT_STRING | SORT_FLAG_CASE);

    return $options;
  }

  /**
   * Helper function to fetch available segments.
   */
  public function getSegments($apiToken)
  {
    // Get available segments.
    $url = 'https://api.lytics.io/v2/segment?table=user';
    $response = \Drupal::httpClient()->get($url, [
      'headers' => [
        'Authorization' => $apiToken,
      ],
    ]);

    $data = json_decode($response->getBody(), TRUE);
    $options = [];

    foreach ($data['data'] as $segment) {
      $options[$segment['slug_name']] = $segment['name'];
    }

    asort($options, SORT_STRING | SORT_FLAG_CASE);

    return $options;
  }
}
