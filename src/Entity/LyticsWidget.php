<?php

namespace Drupal\lytics\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the Lytics Widget entity.
 *
 * @ContentEntityType(
 *   id = "lytics_widget",
 *   label = @Translation("Lytics Widget"),
 *   handlers = {
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\lytics\Form\WidgetManagerForm",
 *       "add" = "Drupal\lytics\Form\WidgetManagerForm",
 *       "edit" = "Drupal\lytics\Form\WidgetManagerForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     }
 *   },
 *   base_table = "lytics_widget",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "title",
 *   },
 *   links = {
 *     "canonical" = "/lytics_widget/{lytics_widget}",
 *     "add-form" = "/lytics_widget/add",
 *     "edit-form" = "/lytics_widget/{lytics_widget}/edit",
 *     "delete-form" = "/lytics_widget/{lytics_widget}/delete",
 *   }
 * )
 */
class LyticsWidget extends ContentEntityBase
{

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type)
  {
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel('ID')
      ->setDescription('Unique ID for the Lytics Widget entity.')
      ->setReadOnly(TRUE);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel('Title')
      ->setDescription('The title of the Lytics Widget entity.')
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ]);

    $fields['description'] = BaseFieldDefinition::create('string_long')
      ->setLabel('Description')
      ->setDescription('The description of the Lytics Widget entity.')
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 1,
      ]);

    $fields['config'] = BaseFieldDefinition::create('string_long')
      ->setLabel('Config')
      ->setDescription('The configuration of the Lytics Widget entity.')
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 2,
      ]);

    $fields['status'] = BaseFieldDefinition::create('string')
      ->setLabel('Status')
      ->setDescription('The status of the Lytics Widget entity.')
      ->setDefaultValue('draft');

    return $fields;
  }

  /**
   * Sets the title of the LyticsWidget entity.
   *
   * @param string $title
   *   The title of the entity.
   */
  public function setTitle($title)
  {
    $this->set('title', $title);
  }

  /**
   * Sets the description of the LyticsWidget entity.
   *
   * @param string $description
   *   The description of the entity.
   */
  public function setDescription($description)
  {
    $this->set('description', $description);
  }

  /**
   * Sets the configuration of the LyticsWidget entity.
   *
   * @param string $config
   *   The configuration of the entity.
   */
  public function setConfiguration($config)
  {
    $this->set('config', $config);
  }

  /**
   * Sets the status of the LyticsWidget entity.
   * 
   * @param string $status
   *  The status of the entity.
   */
  public function setStatus($status)
  {
    $this->set('status', $status);
  }

  /**
   * Gets the title of the LyticsWidget entity.
   *
   * @return string
   *   The title of the entity.
   */
  public function getTitle()
  {
    return $this->get('title')->value;
  }

  /**
   * Gets the description of the LyticsWidget entity.
   *
   * @return string
   *   The description of the entity.
   */
  public function getDescription()
  {
    return $this->get('description')->value;
  }

  /**
   * Gets the configuration of the LyticsWidget entity.
   *
   * @return string
   *   The configuration of the entity.
   */
  public function getConfiguration()
  {
    return $this->get('config')->value;
  }

  /**
   * Gets the status of the LyticsWidget entity.
   *
   * @return string
   *   The status of the entity.
   */
  public function getStatus()
  {
    return $this->get('status')->value;
  }
}
