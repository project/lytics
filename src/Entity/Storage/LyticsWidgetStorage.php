<?php

namespace Drupal\lytics\Entity\Storage;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * Defines the storage handler class for LyticsWidget entities.
 */
class LyticsWidgetStorage extends SqlContentEntityStorage
{
}
