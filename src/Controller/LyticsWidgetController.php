<?php

namespace Drupal\lytics\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\lytics\Entity\LyticsWidget;
use Drupal\Core\Render\Markup;


class LyticsWidgetController extends ControllerBase
{
  public function manageWidgetsListPage()
  {
    $build = [
      '#markup' => '',
    ];

    $widgets = LyticsWidget::loadMultiple();

    $headers = [
      'title' => $this->t('Title'),
      'description' => $this->t('Description'),
      'status' => $this->t('Status'),
      'edit' => $this->t('Edit'),
    ];

    $rows = [];
    foreach ($widgets as $widget) {
      $status = $widget->getStatus();
      $statusColor = $status == 'published' ? '#00D27C' : ($status == 'paused' ? '#FCC504' : '#DBDFE4');
      $editUrl = Url::fromRoute('lytics.manage_web_widgets.edit', ['lytics_widget' => $widget->id()]);
      $rows[] = [
        'title' => $widget->getTitle(),
        'description' => $widget->getDescription(),
        'status' => [
          'data' => [
            '#markup' => Markup::create('<div style="display:inline; padding:5px 8px; background:' . $statusColor . '; border-radius: 5px; text-transform: capitalize; color:#000; font-size:14px; font-weight:600;">' . $widget->getStatus() . '</div>'),
          ],
        ],
        'edit' => [
          'data' => [
            '#type' => 'link',
            '#title' => $this->t('Edit'),
            '#url' => $editUrl,
          ],
        ],
      ];
    }

    $build['existing_widgets'] = [
      '#theme' => 'table',
      '#header' => $headers,
      '#rows' => $rows,
      '#empty' => $this->t('No widgets found.'),
    ];

    return $build;
  }
}
